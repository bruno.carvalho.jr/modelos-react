import React from 'react';
import ReactDom from 'react-dom';
import PrimeiroComponente from './componentes/PrimeiroComponente';
import {ComponenteA, ComponenteB as B} from './componentes/doisComponentesEmUm/DoisComponentesEmUm';
import MultiplosElementos from './componentes/exportarMultElementos/MultiplosElementos';
import Familia from './componentes/dadosDoPaiParaFilho/Familia';
import Membro from './componentes/dadosDoPaiParaFilho/MembroFamilia';
import ComponenteComFuncao from './componentes/componenteComFuncao/ComponenteComFuncao'
import Pai from './componentes/componentesPaiEFilho/Pai'
import ComponenteComClasse from './componentes/componenteClasse/ComponenteClasse'
import Contador from './componentes/alterarPropriedadeSetState/Contador'
import ComponenteFuncaoComEstado from './componentes/componenteFuncaoComEstado/ComponenteFuncaoComEstado'

let element = document.getElementById('root');
ReactDom.render(
    
        <div>
            <ComponenteFuncaoComEstado valor={1}/>
            <Contador numero={150} />
            <ComponenteComClasse valor=""/>
            <Pai />
            <ComponenteComFuncao />
            <Familia sobrenome="Carvalho">
                <Membro nome="Bruno" />
                <Membro nome="Beto" />
                <Membro nome="Rodrigo" />
            </Familia>

            <MultiplosElementos />
            <PrimeiroComponente valor="Valor 1" numero1={1} numero2={2}/>
            <ComponenteA valor="Valor do componente A"/>
            <B valor="Valor do componente B"/>
        </div>
        
, element)