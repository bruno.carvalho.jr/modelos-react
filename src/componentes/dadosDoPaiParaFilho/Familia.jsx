import React from 'react';
import {membrosComProps} from '../../utils/utils'

export default (props) => 
    <div>
        <h1>Membros da familia {props.sobrenome}</h1>
        {membrosComProps(props)}       
    </div>
    