import React from 'react';
import Filho from './Filho'

export default props => {

    const avisandoQueVaiSair = lugar => {
        
        if(lugar) {

            alert(`Liberado para ir para o(a) ${lugar}`)
        }
    }

    return (
        <div>
            <Filho avisandoQueVaiSair={avisandoQueVaiSair} />
        </div>
    )
}
