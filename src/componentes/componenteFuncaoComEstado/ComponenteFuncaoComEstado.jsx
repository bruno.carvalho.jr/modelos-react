import React, {useState, useEffect} from 'react';

//exemplo de componente usando Hook novo metodo, ver documentação os varios hooks que tem
export default props => {

    const [contador, setContador] = useState(props.valor ? props.valor : 0);
    const [status, setStatus] = useState(contador % 2 === 0 ? 'Par' : 'Impar');

    const acrecenta = () => {

        setContador(contador + 1)
    }

    const decrecenta = () => {

        setContador(contador - 1)
    }

    useEffect(() => {

        setStatus(contador % 2 === 0 ? 'Par' : 'Impar')
    })

    return (

        <div>
            <h3>{contador}</h3>
            <h3>{status}</h3>
            <button onClick={() => acrecenta()}> acrecenta</button>
            <button onClick={() => decrecenta()}> decrecenta</button>
        </div>
    )
}