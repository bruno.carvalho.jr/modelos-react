import React from 'react';

export default class ComponenteComClasse extends React.Component {

    render() {

        return(

            <h1>{this.props.valor || 'valor padrão'}</h1>
        )
    }
}