import React from 'react'
    
export default (props) => 
    
    <div>

        <h1> {props.valor} </h1>
        <h3> {props.numero1} </h3>
        <h4> {props.numero2} </h4>
        <p> {props.numero1 * props.numero2} </p>

    </div>

//modo completo
// function primeiro() {

//     return <h1> Primeiro componente </h1>
// }
// export default primeiro;
