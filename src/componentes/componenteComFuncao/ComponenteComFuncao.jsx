import React from 'react';



export default props => {

    const listaDeAprovados = ['Bruno', 'Claudia', 'Vinicius', 'Marcelo']

    const gerarListaDeAprovados = itens => {

        return itens.map(item => <li>{item}</li>)
    }

    return (

        <ul> 
            <h1>Lista de aprovados</h1>
            <br />
            {gerarListaDeAprovados(listaDeAprovados)}
        </ul>
    )
}