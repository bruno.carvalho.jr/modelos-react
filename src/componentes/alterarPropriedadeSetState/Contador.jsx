import React from 'react';

export default class Contador extends React.Component {

    state = {
        numero: this.props.numero
    }

    
    adiciona = () => {
        
        this.setState({numero: this.state.numero + 1})
    }
    
    subtrai= () => {
        
        this.setState({numero: this.state.numero - 1})
    }

    adicionaDiferenca = diferenca => {
        
        this.setState({numero: this.state.numero + diferenca})
    }
    
    render() {
        
        return (
            <div>
                <h3>Numero: {this.state.numero}</h3>
                <button onClick={this.adiciona}>Inc</button> 
                <button onClick={this.subtrai}>Dec</button> 
                <button onClick={() => this.adicionaDiferenca(10)}>Inc</button> 
                <button onClick={() => this.adicionaDiferenca(-10)}>Dec</button> 
            </div>
        )
    }
}
//Soluções para referenciar o this lexo (para o atual local da função)

//Solução 1 - fazendo construtor e o bind na função
// constructor(props) {

//     super(props)
//     this.adiciona = this.adiciona.bind(this);
// }

//Solução 2 - fazendo a arrow function na chamada da função
//<button onClick={() => this.adiciona()}>Inc</button> 

//Solução 3 - transformando a função em uma função arrow
// adiciona = () => {
        
//     // this.props.numero = this.props.numero + 1;
//     console.log(this)
// }
