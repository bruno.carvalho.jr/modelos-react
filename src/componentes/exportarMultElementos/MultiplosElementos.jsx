import React from 'react';

export default props => 
    <div>
        <h3>Parte 1 mult elementos</h3>
        <h3>Parte 2 mult elementos</h3>
    </div>

//metodo 2 de exportar mult elementos
// export default props => 
// <React.Fragment>
//     <h3>Parte 1</h3>
//     <h3>Parte 2</h3>
// </React.Fragment>

//metodo 3 de exportar multi elementos
// export default props => [
//         <h3>Parte 1</h3>,
//         <h3>Parte 2</h3>
// ]